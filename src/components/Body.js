import React, {Component} from "react";
import './Body.css';


class Body extends Component {

  constructor(props) {
    // let p1_cc = "";
    // let p2_cc = "";
    // let result = "";
    super(props);
    const colors = ["red", "green", "brown", "blue"]; 
    this.state = {p1_cc: "", p2_cc: colors[Math.floor(Math.random()*colors.length)], result: ""};
  }

  randomColor = (e) => {
    var colors = ["red", "green", "brown", "blue"]; 
    var p1_cc_i = colors.indexOf(e.target.name);
    var p2_cc_n = colors[Math.floor(Math.random()*colors.length)]
    var p2_cc_i = colors.indexOf(p2_cc_n);

    console.log(e.target.name, p1_cc_i);
    console.log(p2_cc_n, p2_cc_i);
    console.log('----------------------------');

    if ((p1_cc_i - p2_cc_i === -1) || (p1_cc_i - p2_cc_i === 3)) {
      this.setState({p1_cc: e.target.name ,p2_cc: p2_cc_n, result: "Player 1 Win"});
    }

    else if ((p1_cc_i - p2_cc_i === 1) || (p1_cc_i - p2_cc_i === -3)) {
      this.setState({p1_cc: e.target.name ,p2_cc: p2_cc_n, result: "Player 1 Lose"});
    }


    else {
      this.setState({p1_cc: e.target.name ,p2_cc: p2_cc_n, result: "Draw"});
    }
  }


  render () {
    return (
      <div class="grid-container" name="g2">
          <div class="grid-item"></div>
          <div class="grid-item" id="gi3">
            <h1>{this.state.result}</h1>
          </div>
          <div class="grid-item"></div>
          <div class="grid-item" id="gi1">
            <h2>PLAYER 1</h2>
            <button name="red" type="button" class="btn" value="0" onClick={this.randomColor}>RED</button>
            <button name="green" type="button" class="btn" value="1" onClick={this.randomColor}>GREEN</button>
            <button name="brown" type="button" class="btn" value="2" onClick={this.randomColor}>BROWN</button>
            <button name="blue" type="button" class="btn" value="3" onClick={this.randomColor}>BLUE</button>
            <h3 id="p1_cc">{this.state.p1_cc}</h3>
          </div>

          <div class="grid-item">
          </div>

          <div class="grid-item" id="gi2">
          <h2>COMPUTER</h2>
            <button name="red" type="button" class="btn">RED</button>
            <button name="green" type="button" class="btn">GREEN</button>
            <button name="brown" type="button" class="btn">BROWN</button>
            <button name="blue" type="button" class="btn">BLUE</button>
            <h3 id="p2_cc">{this.state.p2_cc}</h3>
          </div>
      </div>
    )
  }
}

export default Body;